
import styled, { css } from 'vue-styled-components';
import { reactive, watch } from '@vue/composition-api';
import { useMousePosition } from '@/hooks/mouse-position/mouse-position.hook';

const Test = {
  name: 'Test',
  setup() {

    const { x, y } = useMousePosition();

    const state = reactive({
      redLabel: false,
      mouse: { x, y, },
    });
    
    return () => (
      <Wrapper>
        <Label value={ {
          red: state.redLabel,
        } }>
          x: { state.mouse.x } / y: { state.mouse.y }
        </Label>
        <Button onClick={ () => state.redLabel = !state.redLabel }>
          { !state.redLabel ? 'redify' : 'unredify' }  
        </Button>
      </Wrapper>
    );
  },
};

const Wrapper = styled.div`
  align-self: center;
  justify-content: center;
  margin-bottom: 10px;
`;

const Label = styled.label`
  align-self: center;
  font-size: 16px;
  line-height: 16px;
  margin-right: 10px;

  ${ ({ value }) => css`
    color: ${ value.red ? 'red' : 'dimgray' };
  ` }
`;

const Button = styled.button`
  align-self: center;
  padding: 10px;
  border-radius: 3px;
`;

export default Test;
