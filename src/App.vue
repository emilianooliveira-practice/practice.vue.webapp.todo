
<template>
  <div id="app">
    <Title />
    <Test />
    <div class="list">
      <Task 
        v-for="task in $options.filters.tasks(tasks)" 
        :key="task.id" 
        :task="task"
        @handle-click="handleTaskClick({task})"
      />
      <form 
        class="new-task-form"
        @submit="handleFormSubmit($event)"
      >
        <NewTaskInput @on-change="handleInputChange" />
        <NewTaskButton />
      </form>
    </div>
    <div class="list -inactive">
      <label class="completed-indicator">
        Completed Tasks
      </label>
      <Task 
        v-for="task in $options.filters.tasks(tasks, active = false)" 
        :key="task.id" 
        :task="task"
        @handle-click="handleTaskClick({task})"
      />
    </div>
  </div>
</template>

<script>
// import HelloWorld from './components/HelloWorld.vue'
import { v4 as uuid } from 'uuid';

// import Title from '@/components/Title/Title.vue'
import Task from '@/components/Task/Task.vue';
import NewTaskButton from '@/components/NewTaskButton/NewTaskButton.vue';
import NewTaskInput from '@/components/NewTaskInput/NewTaskInput.vue';
import Title from '@/components/Title/Title';
import Test from '@/components/Test/Test';

export default {
  name: 'App',
  components: {
    NewTaskInput,
    NewTaskButton,
    Task,
    Title,
    Test,
  },
  data: () => ({
    tasks: [{
      id: uuid(),
      isChecked: true,
      label: 'Task 1',
      lastUpdate: Date.now(),
    }, {
      id: uuid(),
      isChecked: true,
      label: 'Task 2',
      lastUpdate: Date.now(),
    }, {
      id: uuid(),
      isChecked: true,
      label: 'Task 3',
      lastUpdate: Date.now(),
    }, {
      id: uuid(),
      isChecked: false,
      label: 'Task 4',
      lastUpdate: Date.now(),
    }],
    taskLable: '',
  }),
  filters: {
    tasks(list, active = true) {
      return list.filter(({ isChecked }) => {
          return active ? !isChecked : isChecked
        }).sort((a, b) => {
          return b.lastUpdate - a.lastUpdate;
        });
    },
  },
  methods: {
    handleTaskClick ({task}) {
      const index = this.tasks.findIndex(({ id }) => id === task.id);
      this.tasks[index].isChecked = !task.isChecked;
      this.tasks[index].lastUpdate = Date.now();
    },
    handleInputChange (value) {
      this.taskLable = value;
    },
    handleFormSubmit(event) {
      event.preventDefault();
      if(!this.taskLable) alert('Invalid task description.');
      else this.tasks.push({
        id: uuid(),
        label: this.taskLable,
        isChecked: false,
        lastUpdate: Date.now(),
      });
    },
  }
};
</script>

<style lang="scss">
  *, *:before, *:after {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    
    font-family: -apple-system,
    BlinkMacSystemFont,
    "Segoe UI",
    "Roboto",
    "Oxygen",
    "Ubuntu",
    "Cantarell",
    "Fira Sans",
    "Droid Sans",
    "Helvetica Neue",
    sans-serif;
  }
  button {
    &:active, &:focus, &:focus-within, &:hover, &:visited {
      outline: none;
    }
  }
  body {
    overflow: auto;
  }
  img {
    display: block;
  }
  input {
    & ::placeholder {
      color: lightgray;
      opacity: 1;
    }
  }

  #app {
    margin-top: 60px;
    display: flex;
    flex-direction: column;

    .list {
      align-self: center;
      width: 100%;
      max-width: 400px;

      &.-inactive {
        margin-top: 50px;
        opacity: 0.3;
        .completed-indicator {
          color: dimgray;
          align-self: flex-start;
          margin-bottom: 10px;
          display: block;
        }
      }

      .new-task-form {
        margin-top: 30px;
        display: flex;
        height: 40px;
      }
    }
  }
</style>
